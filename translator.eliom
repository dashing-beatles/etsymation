open Listing
open Eliom_lib
open Eliom_content

let translate_listing listing =
  let open Html.D in
  h1 [pcdata listing.title]

let translate_title listing =
  let tmp = listing.title in
  let open Html.D in
  div 
    ~a:[a_id "listing"]
    [h2 ~a:[a_class["subtitle"]]
      [pcdata tmp];
    img ~alt:("Image")
        ~src:(Xml.uri_of_string (listing.url))
        ();
    ]


(* let get_title_html ()  =
  translate_title (Db.get_first_title ()) *)
