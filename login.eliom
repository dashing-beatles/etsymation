[%%shared
open Services
]

let check_pwd id pwd =
    (fun p -> Bcrypt.verify pwd @@ Bcrypt.hash_of_string p) |> Db.get_pwd_from_id_and id

let rec salt s =
  if String.length s > 16
  then s
  else salt @@ s ^ s

let%client is_pwd_ok pwd = String.length pwd > 6

let%client {Notifs.node=notif_node;Notifs.danger;_} = Notifs.make_notification_system ()

(* HTML elements *)

let disconnect_box () = Eliom_content.Html.D.(
  Form.post_form disconnection_service
    (fun _ ->
    [
        Form.input
          ~a:[a_class ["button"]]
          ~input_type:`Submit
          ~value:"Log out" Form.string
    ]
      )
    ()
)

let load_game_link =
  let open Eliom_content.Html.D in
  a Services.host_service [button ~a:[a_class ["button"]] [pcdata "Play"]] ()

[%%shared
type form_mode = Signin | Signup
]

let%shared make_login_form switch_fn =
  let open Eliom_content.Html.D in
  Form.post_form ~service:connection_service
    (fun (name1, pwd) ->
       let name_field, u_name_help, name_input =
         Forms.input ~input_type:`Text ~name:name1 ~l:"Username" Form.string
       in

       let pwd_field, u_pwd_help, pwd_input =
         Forms.input ~input_type:`Password ~name:pwd ~l:"Password" Form.string
       in

       let s_badlogin, u_badlogin =
         Eliom_shared.React.S.create true
       in
       let s_badpwd, u_badpwd =
         Eliom_shared.React.S.create true
       in

       let s_dis_signin =
         Eliom_shared.React.S.l2 [%shared (||)] s_badpwd s_badlogin
       in

       let _ = [%client
       (Lwt_js_events.(async (fun () ->
          let inp = Eliom_content.Html.To_dom.of_input ~%name_input in
          keyups inp (fun _ _ ->
            let s = Js.to_string (inp##.value) in
            if String.length s = 0
            then ( ~%u_badlogin true
                 ; ~%u_name_help (Some "Username field should not be empty", Utils.Danger))
            else ( ~%u_badlogin false
                 ; ~%u_name_help (None,Utils.Primary));
            Lwt.return ())))
        : unit) ]
       in

       let _ = [%client
       (Lwt_js_events.(async (fun () ->
          let inp = Eliom_content.Html.To_dom.of_input ~%pwd_input in
          keyups inp (fun _ _ ->
            let s = Js.to_string (inp##.value) in
            if String.length s = 0
            then ( ~%u_badpwd true
                 ; ~%u_pwd_help (Some "Password field should not be empty", Utils.Danger))
            else ( ~%u_badpwd false
                 ; ~%u_pwd_help (None,Utils.Primary));
            Lwt.return ())))
        : unit) ]
       in

       let switch_btn =
         div ~a:[a_class ["control"]] [
           button
             ~a:[a_class ["button is-link"]
                ;a_button_type `Button
                ;a_onclick [%client (~%switch_fn : Dom_html.mouseEvent Js.t -> unit)]]
             [pcdata "Sign up"]]
       in
       let login_button =
         Form.input
           ~a:[a_class ["button is-success"]
             ;Eliom_content.Html.R.filter_attrib (a_disabled ()) s_dis_signin]
             ~input_type: `Submit ~value:"Login" Form.string in

       [Utils.make_centered_container [
          name_field;
          pwd_field;
          div ~a:[a_class ["level"; "field"]]
            [div ~a:[a_class ["level-left"]]
               [login_button]
            ; div ~a:[a_class ["level-right"]] [
                div ~a:[a_class ["control"]] [
                   switch_btn
        ]]]]]) ()

let%shared make_signup_form switch_fn =
  let open Eliom_content.Html.D in
  Form.post_form ~service:signup_service
    (fun (name, pwd) ->
       let s_badpwd, u_badpwd = Eliom_shared.React.S.create true
       in
       let s_badlogin, u_badlogin = Eliom_shared.React.S.create true
       in
       let s_dis_signup = Eliom_shared.React.S.l2 [%shared (||)] s_badlogin s_badpwd
       in
       let name_field, u_name_help, name_input =
         Forms.input ~l:"Username"
           ~input_type: `Text ~name:name Form.string
       in
       let pwd_field, u_pwd_help, pwd_input =
         Forms.input ~l:"Password"
           ~input_type: `Password ~name:pwd Form.string
       in

       let pwd2_field, u_pwd2_help, pwd2_input =
         Forms.rawinput ~l:"Confirm Password"
           ~input_type: `Password
       in

       let _ = [%client
       (Lwt_js_events.(async (fun () ->
          let inp = Eliom_content.Html.To_dom.of_input ~%pwd2_input in
          let inp2 = Eliom_content.Html.To_dom.of_input ~%pwd_input in
          let verify u_help inp inp2 =
            let s = Js.to_string (inp##.value) in
            let s2 = Js.to_string (inp2##.value) in
            let badpwd = is_pwd_ok s |> not in
            let notsamepwd = s <> s2 in
            ~%u_badpwd (badpwd || notsamepwd);
            u_help (if badpwd
                    then (Some "Password should be 6 characters or more", Utils.Danger)
                    else (if notsamepwd
                          then (Some "Password fields should match", Utils.Danger)
                          else None, Utils.Danger))
          in
          keyups inp (fun _ _ ->
            verify ~%u_pwd_help inp inp2;
            Lwt.return ());
          keyups inp2 (fun _ _ ->
            verify ~%u_pwd2_help inp2 inp;
            Lwt.return ())))
        : unit)
       ] in

       let _ = [%client
       (Lwt_js_events.(async (fun () ->
          let inp = Eliom_content.Html.To_dom.of_input ~%name_input in
          keyups inp (fun _ _ ->
            let s = Js.to_string (inp##.value) in
            ~%u_badlogin (String.length s = 0);
            Lwt.return ())))
        : unit)
       ] in

       let switch_btn =
         div ~a:[a_class ["control"]] [
           button
             ~a:[a_class ["button is-link"]
                ;a_button_type `Button
                ;a_onclick [%client (~%switch_fn : Dom_html.mouseEvent Js.t -> unit)]]
             [pcdata "Sign in"]]
       in

       [Utils.make_centered_container
                [ name_field
                ; pwd_field
                ; pwd2_field
                ; div ~a:[a_class ["field"; "level"]]
                    [ div ~a:[a_class ["level-left"]]
                        [switch_btn]
                    ; div ~a:[a_class ["level-right"]]
                      [ div ~a:[a_class ["control"]]
                          [Form.input
                             ~a:[a_class ["button is-success"]
                                ;Eliom_content.Html.R.filter_attrib (a_disabled ()) s_dis_signup]
                             ~input_type: `Submit ~value:"Sign up" Form.string]]]]
       ]) ()


let connection_box () = Eliom_content.Html.D.(
  let%lwt u = Eliom_reference.get References.username in
  Lwt.return @@
    match u with
    | Some s ->
              Utils.make_centered_container (
                 [p ~a:[a_class ["is-size-1"]] [pcdata "Welcome, "; pcdata s; pcdata "!"];
                    div ~a:[a_class ["field"; "is-grouped"]] [
                    load_game_link;
                    disconnect_box()
                    ]
                 ])
    | None ->
      let s_fmode, u_fmode = Eliom_shared.React.S.create Signin
      in
      let switch_to_login = [%client (fun _ -> ~%u_fmode Signin : Dom_html.mouseEvent Js.t -> unit)]
      in
      let switch_to_signup = [%client (fun _ -> ~%u_fmode Signup : Dom_html.mouseEvent Js.t -> unit)]
      in
      let form_node =
        Eliom_shared.React.S.map
          [%shared fun m ->
              match m with | Signin -> make_login_form ~%switch_to_signup
                           | Signup -> make_signup_form ~%switch_to_login
          ]
          s_fmode
      in
      Eliom_content.Html.R.node form_node
)

(* Service registration *)

(* connect *)
let () =
  Eliom_registration.Action.register
    ~service:connection_service
    (fun _ (name, passwd) ->
       try
         match Db.get_id_from_name name with
         | None ->
           [%client ((danger Eliom_content.Html.D.(pcdata "Username does not exist.")) : unit)];
           Lwt.return_unit
         | Some id ->
           if check_pwd id passwd then
             ( Eliom_reference.set References.username (Some name);
               Eliom_reference.set References.user_id @@ Some id )
           else(
             [%client ((danger Eliom_content.Html.D.(pcdata "Wrong password for this username")) : unit)];
             Lwt.return ())
       with | e -> print_endline "login"; Printexc.print_backtrace (stdout);e |> Printexc.to_string |> print_endline; raise e
    )

(* disconnect *)
let () =
  Eliom_registration.Action.register
    ~service:disconnection_service
    (fun _ _ -> Eliom_state.discard
      ~scope:Eliom_common.default_session_scope ())

(* signup *)
let () =
  Eliom_registration.Action.register
    ~service:signup_service
    (fun _ (name, pwd) -> match Db.get_id_from_name name with
       | None ->
         let pwd = Bcrypt.(hash ~seed:(salt name) pwd |> string_of_hash) in
         let id = Db.new_user_id name pwd in
         Eliom_reference.set References.user_id (Some id);
         Eliom_reference.set References.username (Some name);
         Lwt.return_unit
       | Some _ -> Lwt.return_unit)
