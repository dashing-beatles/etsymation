open Base
open Lwt
open Cohttp
open Cohttp_lwt_unix
open Listing

let url = "https://openapi.etsy.com/v2/listings/active?includes=Images(url_570xN):1:0&fields=title,price,currency_code,listing_id&limit=10&api_key=a8m8epspkfh7turmha9aiyry"

let body = 
  Client.get (Uri.of_string url) >>= fun (resp, body) ->
  body |> Cohttp_lwt.Body.to_string >|= fun body ->
  body

let request () = 
  let body = Lwt_main.run body in
  let json = Yojson.Basic.from_string body in
  let open Yojson.Basic.Util in
  let listings = json |> member "results" |> to_list in
  listings

let first_five, reset =
  let index = ref 0 in
  (fun elt -> if String.equal elt "-" then (true) else (incr index; if !index < 6 then true else false)),
  (fun () -> index := 0)

let load_listing listing =
  let open Yojson.Basic.Util in
  let current_batch = Db.get_current_batch () in
  let title = listing |> member "title" |> to_string
    |> Caml.String.split_on_char ' '
    |> List.filter ~f:first_five
    |> Caml.String.concat " " in
  let price = listing |> member "price" |> to_string |> Float.of_string in
  let currency_code = listing |> member "currency_code" |> to_string in
  let images = listing |> member "Images" |> to_list in
  let url = images |> List.hd_exn |> member "url_570xN" |> to_string in
  let listing_id = listing |> member "listing_id" |> to_int in
  print_endline "loading listing";
  reset();
  {title; price; currency_code; url; listing_id; batch=current_batch}

(* let _ = request () *)