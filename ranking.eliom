
let f name username l =
  let open Eliom_content.Html.D in
  match username with
  | Some login when name = login -> tr ~a:[a_class ["is-selected"]] l
  | Some login -> print_endline login; print_endline name; tr l
  | _ -> tr l

let calc_rate score answer_count =
  let max = answer_count * 100 in
  let x = float_of_int score /. (float_of_int max)
    |> ( *. ) 10000.
    |> ceil
    |> int_of_float
    |> float_of_int
  in
    x /. 100.

let ranking_table scores =
  let open Eliom_content.Html.D in
  let%lwt username = Eliom_reference.get References.username in
  let scores_mapped = List.mapi (fun id (name, score, answer_count, batch_id) ->
    (f name username) [
      td [
        pcdata (string_of_int (id + 1))
      ];
      td [
        pcdata name
      ];
      td [
        pcdata (string_of_int score)
      ];
      td [
        pcdata (string_of_float (calc_rate score answer_count) ^ "%")
      ]
    ]) scores in
  Lwt.return @@ 
  div ~a:[a_class ["columns is-centered"]] [
    div ~a:[a_class ["column is-narrow"]] [
      table ~a:[a_class ["table"]]
        ~thead:(
          thead [
            tr [
              th [pcdata "Rank"];
              th [pcdata "Name"];
              th [pcdata "Score"];
              th [pcdata "correction rate"]
            ]
          ]
        )
      scores_mapped
    ]
  ]

let all_time_table () =
  let scores = Db.get_sorted_scores 0 in
  ranking_table scores

let hourly_table () =
  ranking_table @@ Db.get_sorted_scores (Db.get_current_batch ())

let daily_table () =
  let batch_id = Db.get_current_batch () in
  let q = batch_id mod 24 in
  let min_batch_id = batch_id - q in
  string_of_int min_batch_id |> print_endline;
  ranking_table @@ Db.get_sorted_scores min_batch_id

let () =
  let open Eliom_content.Html.D in
  Eliom_registration.Html.register
    ~service:Services.ranking_service
    (fun _ _ ->
      let%lwt all_time = all_time_table () in
      let%lwt hourly = hourly_table () in
      let%lwt daily = daily_table () in
        Skeleton.skeleton "ranking" @@  [
        section ~a:[a_class["hero"; "is-primary"]] [
          div ~a:[a_class ["hero-body"]] [
            div ~a:[a_class ["container"]] [
              h1 ~a:[a_class["title"]] [
                pcdata "Ranking"
              ];
              p ~a:[a_class[]] [
                pcdata "Find out where you stand!"
              ]
        ]]];
        div ~a:[a_class ["tabs"]] [
          ul [
            li ~a:[a_class ["is-active"]] [
              hourly
            ];
            li [
              daily
            ];
            li [
              all_time
            ]
          ]
        ]
        ]
    )

let _ = Db.get_sorted_scores 0