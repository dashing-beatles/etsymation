(* Update listings *)
(* This should be called once an hour. *)
let update_listings = 
  (fun () () ->
      Db.incr_current_batch ();
      let json = Request.request () in
      let listings = List.map Request.load_listing json in
      List.iter (Db.insert_listing) listings;
      Db.init_guesses_new_batch (List.hd @@ listings).batch;
      Host.refresh_listings listings;
      Db.refresh_play_index ();
      Lwt.return_unit
    )
let update_listings_service =
  Eliom_registration.Action.create
    ~path:Eliom_service.No_path
    ~meth:(Eliom_service.Post (Eliom_parameter.unit, Eliom_parameter.unit))
    update_listings

let update_form =
  let open Eliom_content.Html.D in
  Form.post_form
    ~service:update_listings_service
    (fun () -> [
      div ~a:[a_class ["container"]] [
      Form.button_no_value
        ~a:[a_class["button"]]
        ~button_type: `Submit [pcdata "update"]
    ]])
