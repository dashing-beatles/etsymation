(* Pages *)

let main_service =
  Eliom_service.create
    ~path:(Eliom_service.Path [])
    ~meth:(Eliom_service.Get Eliom_parameter.unit)
    ()

let host_service =
  Eliom_service.create
    ~path:(Eliom_service.Path ["game"])
    ~meth:(Eliom_service.Get Eliom_parameter.unit)
    ()

let ranking_service =
  Eliom_service.create
    ~path:(Eliom_service.Path ["ranking"])
    ~meth:(Eliom_service.Get Eliom_parameter.unit)
    ()

(* Current User Services *)

let connection_service =
  Eliom_service.create
    ~path:Eliom_service.No_path
    ~meth:(Eliom_service.Post (
      Eliom_parameter.unit,
      Eliom_parameter.(string "name" ** string "password")
    ))
    ()

let%client connection_service = ~%connection_service

let disconnection_service =
  Eliom_service.create
    ~path:(Eliom_service.No_path)
    ~meth:(Eliom_service.Post
      (Eliom_parameter.unit, Eliom_parameter.unit))
      ()

let signup_service =
  Eliom_service.create
    ~path:(Eliom_service.No_path)
    ~meth:(Eliom_service.Post (
      Eliom_parameter.unit,
      Eliom_parameter.(string "name" ** string "password")))
    ()

let%client signup_service = ~%signup_service

