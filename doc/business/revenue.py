import matplotlib.pyplot as plt
plt.figure(1)
plt.plot([0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0])
plt.axis([0, 10, 0, 1000])
plt.ylabel('revenue')
plt.xlabel('time (weeks)')
plt.savefig("revenue_large.png")

plt.plot([0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0])
plt.axis([0, 10, 0, 10])
plt.ylabel('revenue')
plt.xlabel('time (weeks)')
plt.savefig("revenue_zoomed.png")