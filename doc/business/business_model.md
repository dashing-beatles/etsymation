# Business model

## Donation

Our business model is based on the generosity of the players.

Here is a curve.

![Revenue on 10 weeks](./business/revenue_large.png){width=50%}

![Revenue on 10 weeks (zoomed)](./business/revenue_zoomed.png){width=50%}

### Final revenue

Our estimation is a revenue of 4€.

Based on the hosting cost, we'll end up at -6€.