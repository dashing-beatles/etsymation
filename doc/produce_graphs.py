import subprocess

stdout = subprocess.run(["find", "-name", "*.dot"], stdout=subprocess.PIPE, universal_newlines=True).stdout
filenames = stdout.split('\n')[:-1]
basenames = [filename[:-3] for filename in filenames]
pngnames = [basename+"png" for basename in basenames]

i = 0
while i < len(basenames):
    png = subprocess.run(["dot", "-Tpng", filenames[i]], stdout=subprocess.PIPE).stdout
    f = open(pngnames[i], "wb")
    f.write(png)
    f.close()
    subprocess.run(["feh", pngnames[i]])
    i = i + 1
