---
institute: Sorbonne Université
title: Etsymation
subtitle: Project Report
author:
- Clément Busschaert
- Alexandre Doussot
course: DAR
teacher: Romain Demangeon
place: Paris
date: \today
header-includes:
  - \usepackage{tikz}
  - \usepackage{listings}
  - \usepackage{color}
  - \usepackage{courier}
  - \usepackage{syntax}
  - \usepackage{fancyvrb}
  - \usepackage{booktabs}
---

\newpage

\makeatletter

\patchcmd{\lsthk@SelectCharTable}{%
  \lst@ifbreaklines\lst@Def{`)}{\lst@breakProcessOther)}\fi
}{%
}{
}{
}

\makeatother

\newcommand{\CodeSymbol}[1]{\textcolor{cyan!50!black}{#1}}
\newcommand{\CodeSymbolPurple}[1]{\textcolor{purple}{#1}}

\lstset{
basicstyle=\footnotesize\ttfamily\bfseries\color{red},
keywordstyle=\color{green!50!black},
keywordstyle=[2]\color{red!50!black},
keywordstyle=[3]\color{purple},
keywordstyle=[4]\color{red},
stringstyle=\color{yellow!80!black},
commentstyle=\itshape\color{gray!50!white},
identifierstyle=\color{cyan!50!black},
literate={\{}{{\CodeSymbol{\{}}}1
               {\}}{{\CodeSymbol{\}}}}1
               {(}{{\CodeSymbol{(}}}1
               {)}{{\CodeSymbol{)}}}1
               {=}{{\CodeSymbol{=}}}1
               {;}{{\CodeSymbol{;}}}1
               {.}{{\CodeSymbol{.}}}1
               {:}{{\CodeSymbol{:}}}1
               {|}{{\CodeSymbol{|}}}1
               {,}{{\CodeSymbol{,}}}1
               {-}{{\CodeSymbol{-}}}1
               {1}{{\CodeSymbol{1}}}1
               {2}{{\CodeSymbol{2}}}1
               {3}{{\CodeSymbol{3}}}1
               {4}{{\CodeSymbol{4}}}1
               {5}{{\CodeSymbol{5}}}1
               {6}{{\CodeSymbol{6}}}1
               {7}{{\CodeSymbol{7}}}1
               {8}{{\CodeSymbol{8}}}1
               {9}{{\CodeSymbol{9}}}1
               {0}{{\CodeSymbol{0}}}1
               {->}{{\CodeSymbol{->}}}1
               {<-}{{\CodeSymbol{<-}}}1
               {+}{{\CodeSymbol{+}}}1
               {-}{{\CodeSymbol{-}}}1
               {*}{{\CodeSymbol{*}}}1
}

\lstdefinelanguage{SOL}{
keywords={},
morestring=[b]{"},
morestring=[b]{'},
morecomment=[l]{//},
morecomment=[s]{/*}{*/},
morecomment=[s][\color{black}]{<}{>}
}

\lstdefinelanguage{JavaScript} {
morekeywords={break,case,catch,continue,debugger,default,delete,do,else,finally,
  for,function,if,in,instanceof,new,return,switch,this,throw,try,typeof,var,void,while, True, False,
  node, with, machine, memory,instances, prototype, list, let, print, type, of},
morekeywords=[2]{interface, class,enum,export,extends,import,super,implements,package,private,protected,public,static,yield},
morekeywords=[3]{True, False, Int, Binop, Not, Or, And, String, Var, Print, Let, If},
morekeywords=[4]{}
morestring=[b]{"},
morestring=[b]{'},
morecomment=[l]{//},
morecomment=[s]{/*}{*/},
morecomment=[s][\color{black}]{<}{>}
}


\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true}
\lstset{escapeinside={(*@}{@*)}}

\newpage

# Concept

Etsymation is a game app, inspired by "The price is right".

## Game core

The goal is to guess the price of the article displayed.
Guess score is then calculated using a formula based on the delta
between the correct price and the guess.
While "The price is right" is fun in its own way, we find
that the added surprise by the sheer unpredictability of the proposed designs.

# Website

The app is comprised of three screens.

## Landing page

This page is comprised of a login form and a signup link, as seen on figure \ref{landing-signin}.
The user can choose either option.
When he is logged in, the app will show a play button (see figure \ref{landing-play}.)

![Landing page with sign-in form\label{landing-signin}](screens/landing_signin.png)

![Landing page with play button\label{landing-play}](screens/landing_play.png)

## Game page

This is the most important page.
It consists of two main elements, presented on figure \ref{game-page}.
The listing information comprises the name of the listing
as well as a picture.
The guess form is made of an input field, a currency information
and the send button.

![Game page\label{game-page}](screens/game_form.png)

When the user is done with the current batch of listings, appears a button
which takes him to the rankings page.

## Rankings page

This is where users can see how they fare compared to others.
It is comprised of three tables, shown in figure \ref{ranks-page},
each showing a ranking for some timeframe.
The way those rankings are calculated is explained in a following
section.

![Rankings page\label{ranks-page}](screens/ranks_page.png)


\newpage
# Current weaknesses

The application in its current state lacks a few user-experience features.

## Security concerns

Most importantly, the server is not configured to work via `https`.
This is easily changeable, but would not fit into our schedule.

There is no way, as of now, to change a user's name, or even password.

Passwords are not encrypted prior to form submission, so travel in clear on the network.

There is no anti-bot captcha system.

These are major security problems rendering the application unsafe to be deployed more or less anywhere.

## Game issues

As we pull our game quiz items from *Etsy!* and do not alter anything from them,
there is absolutely nothing preventing the players to go search on Etsy's website to find the actual price,
and thus give a perfect answer.
A workaround that would be to implement a short timer for each item. Or not showing the item's title.

While providing guesses to the server, the user advances through forms, one page reload per new item,
making the browser registering history steps. After guessing, the back button will take you to the previous page,
which is the same as the one that was reloaded, thus accumulating history items that will show the same page.
This is an issue we have not enough knowledge about browser histories and caches to solve.

## Software problems

We have not managed to optimize the application very well, and have achieved a point
where our client code weighs 4MB, just for a couple form validations and reactive DOM manipulations.

\newpage
# Architecture

![Application architecture\label{arch}](app/architecture.jpg)

The application is a web server providing http services providing HTML content
as well as some headless services to control the server (create users, update informations, etc.)
The term "the client program" from here on will be used to refer to the javascript program
sent as a response to the clients with the HTML.

The figure \ref{arch} shows the global high-level architecture of the application.

## Services

The server provides the following services.
Paths given are relative to the main domain.

### Resource Services

Those are the services that provide HTML content.

#### Main service

| Path | Method | Parameters |
|------|--------|------------|
| /    | GET    | None       |

This delivers the landing page.

#### Game service

| Path  | Method | Parameters |
|-------|--------|------------|
| /game | GET    | None       |

This delivers the game page.

#### Rankings service

| Path     | Method | Parameters |
|----------|--------|------------|
| /ranking | GET    | None       |

This delivers the rankings page.

### User Actions

Those are non-attached services.
They don't have an exact path associated to them,
because they are POST services meant to be accessed via forms only.

#### Sign in action

| Method | Parameters         |
|--------|--------------------|
| POST   | Username, Password |

This signs in the user if the credentials are correct.

#### Sign up action

| Method | Parameters         |
|--------|--------------------|
| POST   | Username, Password |

This creates a new user in the database, if the name is new, and then signs them in.

#### Sign off action

| Method | Parameters |
|--------|------------|
| POST   | None       |

This signs the user off and terminates the session.

### Game Services

These services are the ones used for the game.

#### Send Guess action

| Method | Parameters |
|--------|------------|
| POST   | Guess      |

This sends a guess for the current user's active listing, and updates the scores and the current listing.
If the user is not logged in, this does nothing.

## Database

![Database layout\label{dblayout}](send_guess_service.png){width=70%}

The database, as presented in figure \ref{dblayout}, stores the users and their scores,
in regards to a subset of the informations provided by Etsy for listings.

We only store the url of the listing's main picture, a part of the title of the item, and its price
(as well as the currency it is in.) We take no note of the item's selling store or other sensitive information.

We do store a `play_index` in the users table. This is *hourly persistent* data that stores
where in the current batch the user is at.
It is reset to `0` if the user plays on a new batch 
(regardless of whether they were in the middle of a play session or not.)


\newpage

# Technical decisions

## Why ocsigen.

The initial proposition was to use a Tomcat server in Java,
using raw servlets.
This would have lead to a classic front-end back-end clash
where the Javascript and the Java sides would have to agree on
some conventions and names, without any way to statically verify
that the communications were error-free.
We wanted to distance ourselves from such a workflow
and try something new.
Enter Ocsigen.

### Advantages

Being an OCaml library, Eliom gains a lot from the language's **type system**.
The static verification of both the **server code** and the **client code**
makes it very easy to limit the problems you can have at runtime.

The **tierlessness** of Eliom strengthens it even more with the integration of client-server
communication into the same codebase. It helps designing the application as
one big multi-process program rather than several distinctly separated programs.
There is no front-end and no back-end, with Eliom, as everything sits together
within the same code files.

Using Eliom also eliminates the common headaches that go with deciding on a
client-side Javascript stack. We are compiling OCaml code to Javascript code,
so there is no need for any Angular, Facebook React, or other Vuejs.
Ocsigen and Eliom provide adapted versions of the OCaml library React to allow for
reactive programming and live DOM manipulation.

### Issues

Because the same code produces two different programs that depend on one another,
one of which is then compiled to javascript, a lot of code-location information
is lost during the process and when runtime errors occur,
the debug messages can only state a failure cause, and the actual error
needs to be manually located.
This leads to an extensive amount of time spent bug hunting, even more so if the user
is not experienced with the technology.

Added that the online learning resources are either outdated or scarce,
and the official manual is dauntingly long, understanding the cause of any error
can sometimes be painfully difficult.

To further our pain, there is a clash currently between dependency versions when simply
using the latest `js_of_ocaml-*` packages. We had to pin several packages to an earlier version
just for compatibility's sake.
The tough part is that the clash is not statically detectable,
as the errors appear in the generated Javascript, and thus are unreadable runtime errors
using scrambled unhelpful variable names.

## Database

We used Sqlite for the database management.

We figured learning Eliom would be hard enough
that using unfamiliar tools for the database as well
would slow the development process more than it should.

We were both familiar with Sqlite, so we chose that.

## Deployment

### Attempts at shipping a container

Deploying the application is a task that we had largely underestimated.
Now that cloud hosting services like Microsoft Azure or Amazon Web Services
exist, we thought it really should not be such a difficult task to
install ocaml on a linux server software and run our ocsigen server.
Moreover, every hosting platform encourages the use of docker images.

We thus tried to write a `Dockerfile` for our Ocsigen application.
This endeavour failed, as Ocsigen's own base images are outdated
and OPAM's base images designed for continuous integration are not
suited to setup a web server.
The variants of the `ocaml/opam2` image all setup an `opam` user
which is the only user able to use OPAM and its paths (thus disabling the use of `sudo`),
and should be the owner of any source code, according to the containers' documentation.
But at the time of building the server and installing it on the image,
the `opam` user has no permissions to change the owner of the generated program,
and can't give the web files to the web user `www-data`.

In short, we encountered a lot of unnecessarily complicated system administration issues
when trying to use Docker images.
So we tried something else.

### Selfhosting

As of the writing of this, the application is online, currently running on a Raspberry Pi,
"at home."

There are a lot of issues with this. A Pi is not the best machine to run a web server,
CPU-wise, for one. Secondly, the connection to the server happens via an "end-user" ISP router,
that is designed to be better at downloading than uploading, and isn't necessarily under fiber access.
This renders the connection to the server very slow (slow enough to trigger a time-out in some browser configurations),
and since it's not a powerful machine, if several requests come in simultaneously,
the interaction is going to be even slower.

The fact that our client Javascript code weighs around 4MB doesn't help at all.

\newpage

# Use Cases

## Bob

**Description**:
Bob has finished his exams. He's now free to go back to his entertainment.
He then decides to go back to one of his guilty pleasures, Etsymation.

**Actor**: Client

**Preconditions**:

Bob owns an account ('bobinator': 'hunter2').

Bob's account has a score of 300 and an accuracy of 80% on all**time.

New listings have been loaded.

**Nominal Sequencing**:

1. Bob goes to the Etsymation website.
2. Bob clicks on the login field.
3. Bob fills it with 'bobinator'.
4. Bob clicks on the password field.
5. Bob fills it with 'hunter2'.
6. Bob clicks on login.

**Exception Sequencings**:
E1 : Bob fills an incorrect password.

Sequence starts from step 5 of the nominal sequencing.

  5. Bob fills the password field with  'imwrong'
  6. Une alerte "Incorrect password" is displayed.

**Postconditions**:

Bob is logged in.

The page displays two buttons: `Play` and `Logout`.


**Description**:
Bob has been itching to play lately.
He's going to play.

**Actor**: Client

**Preconditions**:
Bob owns an account ('bobinator': 'hunter2').
Bob is logged in.
Nobody has played on that batch yet.
New listings have been loaded. ('Teddy bear', 20, 'EUR')

**Nominal Sequencing**:

1. Bob clicks on 'Play!'
2. Bob enters 15.
3. Bob clicks on send.
4. Bob clicks on `See my ranking!`

**Exception Sequencings**:
E1 : Bob sends an ill-formed answer.

Sequence starts from step 2 of the nominal sequencing.

  2. Bob enters 'what'.
  3. Bob clicks on send.
  3. An alert 'wrong parameter' is displayed.

**Postconditions**:

On the hourly ranking, Bob is #1.
Bob's score is 95 with an accuracy of 95%

\newpage

# Rankings

## On the site

In order to compare yourself to your friends and the world at large,
Etsymation provides a ranking system. It is accessible at `/ranking`.

As you can see in figure \ref{ranks-page}, there are different rankings based on different timescales.
Let's dive in in how that works.

## Batches

Every hour, the server calls the Etsy API, loading the 10 last listings.
Those are marked with a "batch id" before being loaded in the database.

```sql
table listing (
  id int primary key not null,
  ...
  batch int not null
);
```

These ids are a foreign key of the guess table.


## Guesses

Guesses are stored by a product of `batch_ids x user_id`.

```sql
table guess (
  batch_id int,
  user_id int,
  score int not null,
  answer_count int not null,
  foreign key(batch_id) references listing(batch),
  foreign key(user_id) references users(id),
  unique(batch_id, user_id) on conflict ignore
);
```

When a new batch of listings is loaded, the app creates a
guess entry for each user. When an user sends an answer, the corresponding
guess entry is updated with this new score:

```ocaml
 let index = get_answer_count id batch in
  let index = index + 1 in
  S.execute db
    [%sql "update guess set answer_count = %d where user_id = %L and batch_id = %d"]
    index id batch
```

The question now remains to find which scores we need to sum to get the correct portion of scores corresponding to the timeframe.

## Aggregate

In order to produce rankings for a specific time frame, we need to find
out which `batch_ids` we need to load.
Interestingly, the guesses can be found using sql statements.
Indeed, we only need to load scores from some point in time (i.e from a batch).
This can be done using the following request:
```sql
    select @s{login}, @d{sum(score)}, @d{sum(answer_count)}, @d{batch_id}
    from guess join users on guess.user_id = users.id
    where batch_id >= $batch_id$ and score > 0
    group by login
    order by score desc
```

Now it's only a matter of finding the first batch we need.

### All-time

This one is dead-simple. We need to start from the first one.
```ocaml
  let scores = Db.get_sorted_scores 0 in
```

### Hourly

This one is also simple. We need to start from the last one.

```ocaml
  let scores = Db.get_sorted_scores (Db.get_current_batch ())
```

### Daily

The first batch must have been loaded at midnight. That means that, in order to
find the correct starting batch, we need to take the remainder of the number
of batches and the number of batch per day (24):

```ocaml
  let batch_id = Db.get_current_batch () in
  let q = batch_id mod 24 in
  let min_batch_id = batch_id - q in
  let scores = Db.get_sorted_scores min_batch_id
```

### Monthly

The system is the same, only we do modulo 168 (24*7).

\newpage

# Host


## Back button problems

### The problem

Since Eliom refreshes the page when data changes,
a new entry is added to the browser history.
Thus, when the user presses the back button,
he's presented with the previous listing.

### The fix

In order to prevent the browser from caching
the previous page, we've decided to insert a
call to an eliom reference. Indeed, the reference
is loaded every time, and the current listing
is displayed when the user presses the back button.

## Services

### \purl{host\_service}

This is the main service. It serves the main
page of the game.

![host_service](host/host_service.png){width=70%}

### \purl{send\_guess\_service}

This one is a post service.
Indeed, `send_guess_service` takes as parameter the
guess of the player, and updates his score accordingly.

![send_guess_service](host/send_guess_service.png)
