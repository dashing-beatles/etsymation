# Host


## Back button problems

### The problem

Since Eliom refreshes the page when data changes,
a new entry is added to the browser history.
Thus, when the user presses the back button,
he's presented with the previous listing.

### The fix

In order to prevent the browser from caching
the previous page, we've decided to insert a
call to an eliom reference. Indeed, the reference
is loaded every time, and the current listing
is displayed when the user presses the back button.

## Services

### \purl{host\_service}

This is the main service. It serves the main
page of the game.

![host_service](host/host_service.png){width=70%}

### \purl{send\_guess\_service}

This one is a post service.
Indeed, `send_guess_service` takes as parameter the
guess of the player, and updates his score accordingly.

![send_guess_service](host/send_guess_service.png)
