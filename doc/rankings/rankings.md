# Rankings

## On the site

In order to compare yourself to your friends and the world at large,
Etsymation provides a ranking system. It is accessible at `/ranking`.

As you can see in figure \ref{ranks-page}, there are different rankings based on different timescales.
Let's dive in in how that works.

## Batches

Every hour, the server calls the Etsy API, loading the 10 last listings.
Those are marked with a "batch id" before being loaded in the database.

```sql
table listing (
  id int primary key not null,
  ...
  batch int not null
);
```

These ids are a foreign key of the guess table.


## Guesses

Guesses are stored by a product of `batch_ids x user_id`.

```sql
table guess (
  batch_id int,
  user_id int,
  score int not null,
  answer_count int not null,
  foreign key(batch_id) references listing(batch),
  foreign key(user_id) references users(id),
  unique(batch_id, user_id) on conflict ignore
);
```

When a new batch of listings is loaded, the app creates a
guess entry for each user. When an user sends an answer, the corresponding
guess entry is updated with this new score:

```ocaml
 let index = get_answer_count id batch in
  let index = index + 1 in
  S.execute db
    [%sql "update guess set answer_count = %d where user_id = %L and batch_id = %d"]
    index id batch
```

The question now remains to find which scores we need to sum to get the correct portion of scores corresponding to the timeframe.

## Aggregate

In order to produce rankings for a specific time frame, we need to find
out which `batch_ids` we need to load.
Interestingly, the guesses can be found using sql statements.
Indeed, we only need to load scores from some point in time (i.e from a batch).
This can be done using the following request:
```sql
    select @s{login}, @d{sum(score)}, @d{sum(answer_count)}, @d{batch_id}
    from guess join users on guess.user_id = users.id
    where batch_id >= $batch_id$ and score > 0
    group by login
    order by score desc
```

Now it's only a matter of finding the first batch we need.

### All-time

This one is dead-simple. We need to start from the first one.
```ocaml
  let scores = Db.get_sorted_scores 0 in
```

### Hourly

This one is also simple. We need to start from the last one.

```ocaml
  let scores = Db.get_sorted_scores (Db.get_current_batch ())
```

### Daily

The first batch must have been loaded at midnight. That means that, in order to
find the correct starting batch, we need to take the remainder of the number
of batches and the number of batch per day (24):

```ocaml
  let batch_id = Db.get_current_batch () in
  let q = batch_id mod 24 in
  let min_batch_id = batch_id - q in
  let scores = Db.get_sorted_scores min_batch_id
```

### Monthly

The system is the same, only we do modulo 168 (24*7).
