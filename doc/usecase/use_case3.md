**Description**:
Bob has had a better grade than François in his webdev course.
He's decided to take his revenge. In order to accomplish that,
he will destroy him at his favorite game - Etsymation.

**Actor**: Client

**Preconditions**:
Eve has no account.
Eve wants to create an account (D€$TROY4, ilovememum)

**Nominal Sequencing**:

1. Eve goes to the Etsymation website.
2. Eve clicks on the 'create account' button.
3. She enters 'D€$TROY4' in the login field.
4. She enters 'ilovememum' in the password field.
5. She enters 'ilovememum' in the second password field.
6. She clicks 'create my account'.

**Exception Sequencings**:

E1: The account is already in use.

Sequence starts from step 6 of the nominal sequencing.
    6. She clicks 'create my account'.
    7. A message appear "This login already exists. Please choose another".

**Postconditions**:

Eve is logged in.
The account ('D€$TROY4', 'ilovememum') is in the database.
The page now displays two buttons: 'Play' and 'Logout'
