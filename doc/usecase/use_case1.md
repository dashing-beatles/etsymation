## Bob

**Description**:
Bob has finished his exams. He's now free to go back to his entertainment.
He then decides to go back to one of his guilty pleasures, Etsymation.

**Actor**: Client

**Preconditions**:

Bob owns an account ('bobinator': 'hunter2').

Bob's account has a score of 300 and an accuracy of 80% on all**time.

New listings have been loaded.

**Nominal Sequencing**:

1. Bob goes to the Etsymation website.
2. Bob clicks on the login field.
3. Bob fills it with 'bobinator'.
4. Bob clicks on the password field.
5. Bob fills it with 'hunter2'.
6. Bob clicks on login.

**Exception Sequencings**:
E1 : Bob fills an incorrect password.

Sequence starts from step 5 of the nominal sequencing.

  5. Bob fills the password field with  'imwrong'
  6. Une alerte "Incorrect password" is displayed.

**Postconditions**:

Bob is logged in.

The page displays two buttons: `Play` and `Logout`.