
**Description**:
Bob has been itching to play lately.
He's going to play.

**Actor**: Client

**Preconditions**:
Bob owns an account ('bobinator': 'hunter2').
Bob is logged in.
Nobody has played on that batch yet.
New listings have been loaded. ('Teddy bear', 20, 'EUR')

**Nominal Sequencing**:

1. Bob clicks on 'Play!'
2. Bob enters 15.
3. Bob clicks on send.
4. Bob clicks on `See my ranking!`

**Exception Sequencings**:
E1 : Bob sends an ill-formed answer.

Sequence starts from step 2 of the nominal sequencing.

  2. Bob enters 'what'.
  3. Bob clicks on send.
  3. An alert 'wrong parameter' is displayed.

**Postconditions**:

On the hourly ranking, Bob is #1.
Bob's score is 95 with an accuracy of 95%
