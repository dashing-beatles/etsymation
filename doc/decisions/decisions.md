# Technical decisions

\include decisions/ocsigen.md

\include decisions/db.md

\include decisions/deploy.md
