## Deployment

### Attempts at shipping a container

Deploying the application is a task that we had largely underestimated.
Now that cloud hosting services like Microsoft Azure or Amazon Web Services
exist, we thought it really should not be such a difficult task to
install ocaml on a linux server software and run our ocsigen server.
Moreover, every hosting platform encourages the use of docker images.

We thus tried to write a `Dockerfile` for our Ocsigen application.
This endeavour failed, as Ocsigen's own base images are outdated
and OPAM's base images designed for continuous integration are not
suited to setup a web server.
The variants of the `ocaml/opam2` image all setup an `opam` user
which is the only user able to use OPAM and its paths (thus disabling the use of `sudo`),
and should be the owner of any source code, according to the containers' documentation.
But at the time of building the server and installing it on the image,
the `opam` user has no permissions to change the owner of the generated program,
and can't give the web files to the web user `www-data`.

In short, we encountered a lot of unnecessarily complicated system administration issues
when trying to use Docker images.
So we tried something else.

### Selfhosting

As of the writing of this, the application is online, currently running on a Raspberry Pi,
"at home."

There are a lot of issues with this. A Pi is not the best machine to run a web server,
CPU-wise, for one. Secondly, the connection to the server happens via an "end-user" ISP router,
that is designed to be better at downloading than uploading, and isn't necessarily under fiber access.
This renders the connection to the server very slow (slow enough to trigger a time-out in some browser configurations),
and since it's not a powerful machine, if several requests come in simultaneously,
the interaction is going to be even slower.

The fact that our client Javascript code weighs around 4MB doesn't help at all.
