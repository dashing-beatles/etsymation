## Why ocsigen.

The initial proposition was to use a Tomcat server in Java,
using raw servlets.
This would have lead to a classic front-end back-end clash
where the Javascript and the Java sides would have to agree on
some conventions and names, without any way to statically verify
that the communications were error-free.
We wanted to distance ourselves from such a workflow
and try something new.
Enter Ocsigen.

### Advantages

Being an OCaml library, Eliom gains a lot from the language's **type system**.
The static verification of both the **server code** and the **client code**
makes it very easy to limit the problems you can have at runtime.

The **tierlessness** of Eliom strengthens it even more with the integration of client-server
communication into the same codebase. It helps designing the application as
one big multi-process program rather than several distinctly separated programs.
There is no front-end and no back-end, with Eliom, as everything sits together
within the same code files.

Using Eliom also eliminates the common headaches that go with deciding on a
client-side Javascript stack. We are compiling OCaml code to Javascript code,
so there is no need for any Angular, Facebook React, or other Vuejs.
Ocsigen and Eliom provide adapted versions of the OCaml library React to allow for
reactive programming and live DOM manipulation.

### Issues

Because the same code produces two different programs that depend on one another,
one of which is then compiled to javascript, a lot of code-location information
is lost during the process and when runtime errors occur,
the debug messages can only state a failure cause, and the actual error
needs to be manually located.
This leads to an extensive amount of time spent bug hunting, even more so if the user
is not experienced with the technology.

Added that the online learning resources are either outdated or scarce,
and the official manual is dauntingly long, understanding the cause of any error
can sometimes be painfully difficult.

To further our pain, there is a clash currently between dependency versions when simply
using the latest `js_of_ocaml-*` packages. We had to pin several packages to an earlier version
just for compatibility's sake.
The tough part is that the clash is not statically detectable,
as the errors appear in the generated Javascript, and thus are unreadable runtime errors
using scrambled unhelpful variable names.
