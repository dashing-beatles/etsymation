## Database

We used Sqlite for the database management.

We figured learning Eliom would be hard enough
that using unfamiliar tools for the database as well
would slow the development process more than it should.

We were both familiar with Sqlite, so we chose that.
