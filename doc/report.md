---
institute: Sorbonne Université
title: Etsymation
subtitle: Project Report
author:
- Clément Busschaert
- Alexandre Doussot
course: DAR
teacher: Romain Demangeon
place: Paris
date: \today
header-includes:
  - \usepackage{tikz}
  - \usepackage{listings}
  - \usepackage{color}
  - \usepackage{courier}
  - \usepackage{syntax}
  - \usepackage{fancyvrb}
  - \usepackage{booktabs}
---

\newpage

\include tex_templates/code.tex

\newpage

\include app/app.md

\newpage

\include decisions/decisions.md

\newpage

\include usecase/usecase.md

\newpage

\include rankings/rankings.md

\newpage

\include host/host.md
