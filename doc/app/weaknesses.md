# Current weaknesses

The application in its current state lacks a few user-experience features.

## Security concerns

Most importantly, the server is not configured to work via `https`.
This is easily changeable, but would not fit into our schedule.

There is no way, as of now, to change a user's name, or even password.

Passwords are not encrypted prior to form submission, so travel in clear on the network.

There is no anti-bot captcha system.

These are major security problems rendering the application unsafe to be deployed more or less anywhere.

## Game issues

As we pull our game quiz items from *Etsy!* and do not alter anything from them,
there is absolutely nothing preventing the players to go search on Etsy's website to find the actual price,
and thus give a perfect answer.
A workaround that would be to implement a short timer for each item. Or not showing the item's title.

While providing guesses to the server, the user advances through forms, one page reload per new item,
making the browser registering history steps. After guessing, the back button will take you to the previous page,
which is the same as the one that was reloaded, thus accumulating history items that will show the same page.
This is an issue we have not enough knowledge about browser histories and caches to solve.

## Software problems

We have not managed to optimize the application very well, and have achieved a point
where our client code weighs 4MB, just for a couple form validations and reactive DOM manipulations.
