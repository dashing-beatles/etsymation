# Concept

Etsymation is a game app, inspired by "The price is right".

## Game core

The goal is to guess the price of the article displayed.
Guess score is then calculated using a formula based on the delta
between the correct price and the guess.
While "The price is right" is fun in its own way, we find
that the added surprise by the sheer unpredictability of the proposed designs.

# Website

The app is comprised of three screens.

## Landing page

This page is comprised of a login form and a signup link, as seen on figure \ref{landing-signin}.
The user can choose either option.
When he is logged in, the app will show a play button (see figure \ref{landing-play}.)

![Landing page with sign-in form\label{landing-signin}](screens/landing_signin.png)

![Landing page with play button\label{landing-play}](screens/landing_play.png)

## Game page

This is the most important page.
It consists of two main elements, presented on figure \ref{game-page}.
The listing information comprises the name of the listing
as well as a picture.
The guess form is made of an input field, a currency information
and the send button.

![Game page\label{game-page}](screens/game_form.png)

When the user is done with the current batch of listings, appears a button
which takes him to the rankings page.

## Rankings page

This is where users can see how they fare compared to others.
It is comprised of three tables, shown in figure \ref{ranks-page},
each showing a ranking for some timeframe.
The way those rankings are calculated is explained in a following
section.

![Rankings page\label{ranks-page}](screens/ranks_page.png)


\newpage
\include app/weaknesses.md

\newpage
\include app/architecture.md
