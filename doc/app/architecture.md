# Architecture

![Application architecture\label{arch}](app/architecture.jpg)

The application is a web server providing http services providing HTML content
as well as some headless services to control the server (create users, update informations, etc.)
The term "the client program" from here on will be used to refer to the javascript program
sent as a response to the clients with the HTML.

The figure \ref{arch} shows the global high-level architecture of the application.

## Services

The server provides the following services.
Paths given are relative to the main domain.

### Resource Services

Those are the services that provide HTML content.

#### Main service

| Path | Method | Parameters |
|------|--------|------------|
| /    | GET    | None       |

This delivers the landing page.

#### Game service

| Path  | Method | Parameters |
|-------|--------|------------|
| /game | GET    | None       |

This delivers the game page.

#### Rankings service

| Path     | Method | Parameters |
|----------|--------|------------|
| /ranking | GET    | None       |

This delivers the rankings page.

### User Actions

Those are non-attached services.
They don't have an exact path associated to them,
because they are POST services meant to be accessed via forms only.

#### Sign in action

| Method | Parameters         |
|--------|--------------------|
| POST   | Username, Password |

This signs in the user if the credentials are correct.

#### Sign up action

| Method | Parameters         |
|--------|--------------------|
| POST   | Username, Password |

This creates a new user in the database, if the name is new, and then signs them in.

#### Sign off action

| Method | Parameters |
|--------|------------|
| POST   | None       |

This signs the user off and terminates the session.

### Game Services

These services are the ones used for the game.

#### Send Guess action

| Method | Parameters |
|--------|------------|
| POST   | Guess      |

This sends a guess for the current user's active listing, and updates the scores and the current listing.
If the user is not logged in, this does nothing.

## Database

![Database layout\label{dblayout}](send_guess_service.png){width=70%}

The database, as presented in figure \ref{dblayout}, stores the users and their scores,
in regards to a subset of the informations provided by Etsy for listings.

We only store the url of the listing's main picture, a part of the title of the item, and its price
(as well as the currency it is in.) We take no note of the item's selling store or other sensitive information.

We do store a `play_index` in the users table. This is *hourly persistent* data that stores
where in the current batch the user is at.
It is reset to `0` if the user plays on a new batch 
(regardless of whether they were in the middle of a play session or not.)

