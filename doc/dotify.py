### current bpython session - make changes and save to reevaluate session.
### lines beginning with ### will be ignored.
### To return to bpython without reevaluating make no changes to this file
### or save an empty file.
import sys

filename = "../create_db.sqlite"
f = open(filename, "r")
lines = f.read().split("\n")

start = ""
end = ""
tables = ""
edges = ""
self_table = ""

for line in lines:
    line = line.split(" ")
    line = [ elt.strip(",") for elt in line if elt != " " and elt != '']
    if len(line) == 0:
        continue
    elif line[0] == ');':
        tables += start + end + "\n"
        start = ""
        end = ""
        self_table = ""
    elif line[0] == 'create':
        self_table = line[2]
        start += line[2] + '[label=<\n<table border="0" cellspacing="1" width="134">\n<tr><td height="24" valign="bottom"><b>'\
        + line[2] + '</b></td></tr>\n<hr/>\n'
        end = '</table>>, border=0];' + end
    elif line[0] == 'foreign':
        ext_table = line[3].split('(')[0]
        key = line[1].split('(')[1].strip(')')
        edges += self_table + " -> " + ext_table + "; \n"
        start += '<tr><td align="left"><i>' + key + '</i></td></tr>'
    elif "primary" in line:
        key = line[0]
        start += '<tr><td align="left"><u>' + key + '</u></td></tr>'
    else:
        start += '<tr><td align="left">'+ line[0] + '<b> ' + " ".join(line[1:]) + "</b></td></tr>\n"

style = """
digraph {
    graph [
    fontname="helvetica"];
    node [
    shape="Mrecord",
    fontsize=12,
    fontname="helvetica",
    margin="0.07,0.04",
    penwidth="1.0"];
"""
print(style)
print(tables)
print(edges)
print("}")
