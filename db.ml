open Listing

module Sqlexpr = Sqlexpr_sqlite.Make(Sqlexpr_concurrency.Id)
module S = Sqlexpr
module Vol = Eliom_reference.Volatile

let db_name = "foo.db"

let init () =
  print_endline "whaaaaaaat";
  let db = S.open_db db_name in
  S.iter db
    (fun name -> print_endline name;)
    [%sql "select @s{title} from listing"];
  print_endline "the end."

let insert_listing listing =
  let db = S.open_db db_name in
  S.execute db
    [%sqlc "INSERT or IGNORE INTO listing VALUES(%d, %s, %f, %s, %s, %d)"]
    listing.listing_id listing.title listing.price listing.url listing.currency_code listing.batch

let get_titles () =
  let db = S.open_db db_name in
  S.select db [%sql "select @s{title}, @s{url}, @s{currency_code}, @f{price} from listing"]

let index_ref = Vol.eref Eliom_common.default_session_scope 0

(* let get_first_title () =
  let titles = get_titles () in
  let index = Vol.get index_ref in
  Vol.set index_ref (index + 1);
  let title, url, currency_code, price, batch =
    List.nth titles (index)
  in
  {listing_id=0; title; price; currency_code; url; batch} *)

let insert_user_stmt =
  [%sqlc "insert into users(login, passwd, play_index) values(%s, %s, %d)"]

let insert_user ~login ~pwd =
  let db = S.open_db db_name in
  S.execute db insert_user_stmt login pwd 0

let new_user_id login pwd =
  let db = S.open_db db_name in
  S.insert db insert_user_stmt login pwd 0

let get_id_from_name name =
  let db = S.open_db db_name in
  S.select_one_maybe db [%sql "select @L{id} from users where login = %s"] name

let get_pwd_from_id_and id f =
  let db = S.open_db db_name in
  S.select_one_f db f [%sql "select @s{passwd} from users where id = %L"] id

let refresh_play_index () =
  let db = S.open_db db_name in
  S.execute db [%sqlc "update users set play_index = 0"]

let get_play_index id =
  let db = S.open_db "foo.db" in
  S.select_one db [%sql "select @d{play_index} from users where id = %L"] id

let get_score id =
  let db = S.open_db "foo.db" in
  let score = S.select_one db [%sql "select @d{score} from users where id = %L"] id in
  score

let update_score id batch delta =
  try
  print_int batch;
  let db = S.open_db "foo.db" in
  let score = S.select_one db
    [%sql "select @d{score} from guess where user_id = %L and batch_id = %d"] id batch in
  let score = score + delta in
  S.execute db [%sql "update guess set score = %d where user_id = %L and batch_id = %d"]
    score id batch
  with
  | _ as e -> print_endline "score"; Printexc.print_backtrace (stdout);e |> Printexc.to_string |> print_endline; raise e

let get_sorted_scores min_batch_id =
  let db = S.open_db db_name in
  let scores = S.select db
    [%sql "select @s{login}, @d{sum(score)}, @d{sum(answer_count)}, @d{batch_id}
      from guess join users on guess.user_id = users.id
      where batch_id >= %d and score > 0
      group by login
      order by score desc"]
        min_batch_id in
  scores

let get_total_score user_id =
  let db = S.open_db db_name in
  let score = S.select_one db
    [%sql "select @d{sum(score)} 
      from guess join users on guess.user_id = users.id
      where users.id = %L and batch_id >= 0 and score > 0
      group by login
      order by score desc"]
        user_id in
  score

let incr_play_index id =
  let db = S.open_db "foo.db" in
  let index = get_play_index id in
  let index = index + 1 in
  S.execute db [%sql "update users set play_index = %d where id = %L"] index id

let get_answer_count id batch =
  let db = S.open_db "foo.db" in
  S.select_one db
    [%sql "select @d{answer_count} from guess where user_id = %L and batch_id = %d"]
    id batch

let update_answer_count id batch =
  let db = S.open_db "foo.db" in
  let index = get_answer_count id batch in
  let index = index + 1 in
  S.execute db
    [%sql "update guess set answer_count = %d where user_id = %L and batch_id = %d"]
    index id batch

let insert_guess_stmt =
  [%sqlc "insert into guess(batch_id, user_id, score, answer_count) values(%d, %L, 0, 0)"]

let init_guesses_new_batch batch_id =
  let db = S.open_db "foo.db" in
  let ids = S.select db [%sqlc "select @L{id} from users"] in
  ids
    |> List.iter (fun user_id ->
      S.execute db insert_guess_stmt batch_id user_id
    )

let get_current_batch () =
  let db = S.open_db db_name in
  S.select_one db
    [%sqlc "select @d{current_batch} from batch"]

let incr_current_batch () =
  let db = S.open_db db_name in
  let current_batch = get_current_batch () + 1 in
  S.execute db
    [%sqlc "update batch set current_batch = %d"] current_batch