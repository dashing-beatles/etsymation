

let footer_f =
  let open Eliom_content.Html.F in
  [footer ~a:[a_class ["footer"]] [
    div ~a:[a_class ["content"; "has-text-centered"]] [
      p [
        pcdata "copyleft dar 2018 - Made by Clément Busschaert and Alexandre Doussot"
      ];
      a Services.host_service [pcdata "Play!         "] ();
      a Services.ranking_service [pcdata "Rankings!"] ();
    ]
  ]
  ]

let footer_d =
  let open Eliom_content.Html.D in
  [footer ~a:[a_class ["footer"]] [
    div ~a:[a_class ["content"; "has-text-centered"]] [
      p [
        pcdata "copyleft dar 2018 - Made by Clément Busschaert and Alexandre Doussot"
      ];
      a Services.host_service [pcdata "Play!         "] ();
      a Services.ranking_service [pcdata "Rankings!"] ();
    ]
  ]
  ]

    
let skeleton_f title body_content =
  let open Eliom_content.Html.F in
  Lwt.return @@
    Eliom_tools.F.html
      ~title:"etsymation"
      ~css:[["css"; "style.css"]]
      (body (body_content @ footer_f))

let skeleton title body_content =
  let open Eliom_content.Html.D in
  Lwt.return @@
    Eliom_tools.D.html
      ~title:"etsymation"
      ~css:[["css"; "style.css"]]
      (body (body_content @ footer_d))