let username =
  let user_opt : string option = None in
  Eliom_reference.eref
    ~scope:Eliom_common.default_session_scope user_opt

(* FIXME: update correctly this reference *)
let user_id =
  Eliom_reference.eref
    ~scope:Eliom_common.default_session_scope (None : int64 option)

let user_play_index =
  Eliom_reference.eref
    ~scope:Eliom_common.default_session_scope 0
