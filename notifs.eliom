[%%client
type notif_system =
  { node: Html_types.div_content Eliom_content.Html.elt
  ; info: Html_types.div_content Eliom_content.Html.elt -> unit
  ; success: Html_types.div_content Eliom_content.Html.elt -> unit
  ; warning: Html_types.div_content Eliom_content.Html.elt -> unit
  ; danger: Html_types.div_content Eliom_content.Html.elt -> unit
  ; dismiss: unit -> unit
  }

type notif_type
  = Info
  | Warning
  | Success
  | Danger

let is_info, is_success, is_warning, is_danger =
  ((=) Info, (=) Success, (=) Warning, (=) Danger)

let type_classes tys =
  Eliom_content.Html.(
    [ R.filter_attrib (D.a_class ["is-info"]) @@ React.S.map is_info tys
    ; R.filter_attrib (D.a_class ["is-success"]) @@ React.S.map is_success tys
    ; R.filter_attrib (D.a_class ["is-warning"]) @@ React.S.map is_warning tys
    ; R.filter_attrib (D.a_class ["is-danger"]) @@ React.S.map is_danger tys
    ])

let make_notification_system () =
  let (s_notif : (Html_types.div_content Eliom_content.Html.elt option * notif_type) Eliom_shared.React.S.t)
    , u_notif = React.S.create (None,Info)
  in
  let s_message = React.S.map fst s_notif
  in
  let s_type = React.S.map snd s_notif
  in
  let delete_button = Eliom_content.Html.D.(button ~a:[a_class ["delete"]; a_onclick (fun _ -> u_notif (None,Info))] [])
  in
  let notif_react : Html_types.div_content Eliom_content.Html.elt Eliom_shared.React.S.t =
    React.S.map Eliom_content.Html.(function
      | None -> D.div []
      | Some content ->
        D.div ~a:([D.a_class ["notification"]] @ type_classes s_type)
          [delete_button; content]
    ) s_message
  in
  { node = Eliom_content.Html.R.node notif_react
  ; info = (fun m -> u_notif (Some m, Info))
  ; danger = (fun m -> u_notif (Some m, Danger))
  ; success = (fun m -> u_notif (Some m, Success))
  ; warning = (fun m -> u_notif (Some m, Warning))
  ; dismiss = (fun () -> u_notif (None, Info))}
]
