[%%shared.start]
let make_container t =
  let open Eliom_content.Html.D in
  [ div ~a:[a_class ["container"]] t]


let make_centered_container t =
  let open Eliom_content.Html.D in
    div ~a:[a_class ["container"]] [
       div ~a:[a_class ["columns"]] [
         div ~a:[a_class ["column"; "is-three-fifths"; "is-offset-one-fifth"]]
           t
       ]
    ]

(* [%%shared.start] *)
type color
  = Primary
  | Link
  | Info
  | Success
  | Warning
  | Danger

let string_of_color = function
  | Primary -> "is-primary"
  | Link -> "is-link"
  | Info -> "is-info"
  | Success -> "is-success"
  | Warning -> "is-warning"
  | Danger -> "is-danger"

let class_of_color color = [color |> string_of_color] |> Eliom_content.Html.D.a_class

let is_primary, is_link, is_info, is_success, is_warning, is_danger =
  let is_color color = function | None, _ -> false | (_ : string option), c -> (c = color)
  in
  ( is_color Primary
  , is_color Link
  , is_color Info
  , is_color Success
  , is_color Warning
  , is_color Danger
  )

(* [%%server.start] *)
let reactive_class_of_color s_color =
  let open Eliom_shared in
  let open Eliom_content.Html in
  [ R.filter_attrib (class_of_color Primary) @@ React.S.map [%shared ((=) Primary)] s_color
  ; R.filter_attrib (class_of_color Link) @@ React.S.map [%shared ((=) Link)] s_color
  ; R.filter_attrib (class_of_color Info) @@ React.S.map [%shared ((=) Info)] s_color
  ; R.filter_attrib (class_of_color Success) @@ React.S.map [%shared ((=) Success)] s_color
  ; R.filter_attrib (class_of_color Warning) @@ React.S.map [%shared ((=) Warning)] s_color
  ; R.filter_attrib (class_of_color Danger) @@ React.S.map [%shared ((=) Danger)] s_color
  ]

let reactive_class_of_colored_message s =
  let open Eliom_shared in
  let open Eliom_content.Html in
  [ R.filter_attrib (class_of_color Primary) @@ React.S.map [%shared is_primary ] s
  ; R.filter_attrib (class_of_color Link) @@ React.S.map [%shared is_link] s
  ; R.filter_attrib (class_of_color Info) @@ React.S.map [%shared is_info ] s
  ; R.filter_attrib (class_of_color Success) @@ React.S.map [%shared is_success ] s
  ; R.filter_attrib (class_of_color Warning) @@ React.S.map [%shared is_warning ] s
  ; R.filter_attrib (class_of_color Danger) @@ React.S.map [%shared is_danger ] s
  ]
