[%%shared.start]
val help_node_of_text :
  string option * Utils.color ->
  [> Html_types.p] Eliom_content.Html.elt

(* [%%server.start] *)
val input : l:string
  -> input_type:[< Html_types.input_type ]
  -> name:[< 'a Eliom_parameter.setoneradio ] Eliom_parameter.param_name
  -> 'a Eliom_content.Html.form_param
  -> Html_types.form_content Eliom_content.Html.elt
     * (?step:React.step -> string option * Utils.color -> unit) Eliom_shared.Value.t
     * [> Html_types.input ] Eliom_content.Html.elt

val rawinput : l:string
  -> input_type:[< Html_types.input_type ]
  -> [> Html_types.div ] Eliom_content.Html.elt
     * (?step:React.step -> string option * Utils.color -> unit) Eliom_shared.Value.t
     * [> Html_types.input ] Eliom_content.Html.elt
