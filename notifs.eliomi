[%%client.start]

(* The type for notification systems. *)
type notif_system =
  { node: Html_types.div_content Eliom_content.Html.elt
        (* The reactive node hosting the notification itself *)
  ; info: Html_types.div_content Eliom_content.Html.elt -> unit
      (* a function to display an 'info' notification in the node *)
  ; success: Html_types.div_content Eliom_content.Html.elt -> unit
      (* a function to display a 'success' notification in the node *)
  ; warning: Html_types.div_content Eliom_content.Html.elt -> unit
      (* a function to display a 'warning' notification in the node *)
  ; danger: Html_types.div_content Eliom_content.Html.elt -> unit
      (* a function to display a 'danger' notification in the node *)
  ; dismiss: unit -> unit
      (* a function to dismiss the current notification *)
  }

val make_notification_system:
  unit -> notif_system
