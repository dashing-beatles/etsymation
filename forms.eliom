[%%shared
open Eliom_content.Html
open Utils


let help_node_of_text (opt,ty) =
  let open D in
  match opt with
  | None ->
    p []
  | Some msg ->
    p ~a:[a_class ["help"]; class_of_color ty]
      [pcdata msg]

let input ~l ~input_type ~name ty =
  let open Eliom_shared.React in
  let s_help, u_help = S.create (None, Primary) in
  let s_help_node = S.map [%shared help_node_of_text] s_help in
  let open D in
  let input_node = Form.input
        ~a:([a_class ["input"]] @ reactive_class_of_colored_message s_help)
        ~input_type:input_type ~name:name ty
  in
  ( div ~a:[a_class ["field"]]
      [ label ~a:[a_class ["label"]] [pcdata l]
      ; div ~a:[a_class ["control"]] [input_node]
      ; R.node s_help_node
      ]
  , u_help
  , input_node
  )

let rawinput ~l ~input_type =
  let open Eliom_shared.React in
  let s_help, u_help = S.create (None, Primary) in
  let s_help_node = S.map [%shared help_node_of_text] s_help in
  let open D in
  let input_node = Raw.input
      ~a:([a_input_type input_type
          ;a_class ["input"]] @ reactive_class_of_colored_message s_help)
      ()
  in
  ( div ~a:[a_class ["field"]]
      [ label ~a:[a_class ["label"]] [pcdata l]
      ; div ~a:[a_class ["control"]] [input_node]
      ; R.node s_help_node
      ]
  , u_help
  , input_node
  )

]
