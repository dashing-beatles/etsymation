type listing = {
  listing_id: int;
  title : string;
  price : float;
  currency_code: string;
  url: string;
  batch: int
  }
 (* [@@deriving json] *)