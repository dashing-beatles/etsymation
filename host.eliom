open Listing

let listing = {listing_id = 0; title="Dummy"; price=6.9; currency_code="NOP"; url=""; batch=(-1)}
let current_listings = ref (Array.make 10 listing)

let refresh_listings listings =
  let listings_array = Array.of_list listings in
  current_listings := listings_array;
  print_endline "Done loading listings"

let send_guess_service =
  Eliom_registration.Action.create
    ~path:Eliom_service.No_path
    ~meth:(Eliom_service.Post (Eliom_parameter.unit, Eliom_parameter.float "guess"))
    (fun () guess ->
       try
       let%lwt user_id = Eliom_reference.get References.user_id in
       match user_id with
       | None -> Lwt.return_unit
       | Some user_id ->
         let play_index = Db.get_play_index user_id in
         let listing = !current_listings.(play_index) in
         let diff = listing.price -. guess |> abs_float |> max 0. in
         let score = floor (100.0 -. diff +. 0.5) |> int_of_float in
         (* This is so ugly *)
         Db.update_score user_id listing.batch score;
         Db.update_answer_count user_id listing.batch;
         Db.incr_play_index user_id;
         Eliom_reference.set References.user_play_index (play_index + 1);
         Lwt.return_unit
      with
      | _ as e -> Printexc.print_backtrace (stdout);e |> Printexc.to_string |> print_endline; raise e
    )

let previous_answer answer_opt =
  let open Eliom_content.Html.D in
  match answer_opt with
  | None -> div []
  | Some (price, currency) -> p [pcdata ("It was " ^ (string_of_float price) ^ " " ^ currency)]

let get_previous_answer_opt play_index =
  match play_index with
  | 0 -> None
  | n -> let prev_listing = (!current_listings.(n - 1)) in
        Some (prev_listing.price, prev_listing.currency_code)

let guess_price_form () =
    try
  let open Eliom_content.Html.D in
  let%lwt user_id = Eliom_reference.get References.user_id in
  match user_id with
  | None ->
    let%lwt connect = Login.connection_box() in
      Lwt.return @@
    div [
      p [
        pcdata "You can't guess if not logged in"
      ];
      a Services.main_service [pcdata "Log in!"] ();
    ];
  | Some user_id ->
    let%lwt play_index = Eliom_reference.get References.user_play_index in
    let previous_answer_opt = get_previous_answer_opt play_index in
    Lwt.return @@
    match play_index with
    | 10 -> div [
            previous_answer previous_answer_opt;
            p [
              pcdata "No more for today, come back later!"
            ];
            a Services.ranking_service [button ~a:[a_class ["button"]] [pcdata "See my ranking"]] ()
          ]
    | _ ->
      let listing = !current_listings.(play_index) in
      let listing_html = Translator.translate_title listing in
      Form.post_form
        ~service:send_guess_service
        (fun guess_name -> [
            h1 ~a:[a_class ["title"]] [pcdata "And we're now live!"];
            listing_html;
            previous_answer previous_answer_opt;
            div ~a:[a_class ["field"; "has-addons"]] [
              div ~a:[a_class ["control"]] [
                Form.input ~a:[a_class ["input"]]
                  ~input_type:`Text
                  ~name:guess_name Form.float
              ];
              p ~a:[a_class ["control"; "button"; "is-static"]] [
                pcdata listing.currency_code
              ];
              Form.button_no_value
                ~a:[a_class ["button is-info"]]
                ~button_type:`Submit
                [pcdata "Send guess"]
            ];
           ]) ()
    with
    | e -> Printexc.to_string e |> print_endline; raise e

(* Game page *)

let () =
  Eliom_registration.Html.register
    ~service:Services.host_service
    (fun _ _ ->
      let open Eliom_content.Html.D in
      let%lwt guess_form = guess_price_form () in
      let%lwt user_id = Eliom_reference.get References.user_id in
      (match user_id with
      | Some user_id ->
      let play_index = Db.get_play_index user_id in
      Eliom_reference.set References.user_play_index play_index
      | None -> Lwt.return_unit);

      Skeleton.skeleton "game" @@
      [
        section ~a:[a_class["hero"; "is-primary"]] [
          div ~a:[a_class ["hero-body"]] [
            div ~a:[a_class ["container"]] [

      guess_form]]]]
    )
