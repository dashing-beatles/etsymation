open Eliom_content.Html.F

(* -------------------------------------------------------- *)
(* We create two main services on the same URL,             *)
(* one with a GET integer parameter:                        *)

let calc = Eliom_service.create
    ~path:(Eliom_service.Path ["calc"])
    ~meth:(Eliom_service.Get Eliom_parameter.unit) ()
let calc_i =
  Eliom_service.create
    ~path:(Eliom_service.Path ["calc"])
    ~meth:(Eliom_service.Get (Eliom_parameter.int "i"))
    ()

(* -------------------------------------------------------- *)
(* The handler for the service without parameter.           *)
(* It displays a form where you can write an integer value: *)

let calc_handler () () =
  let create_form intname =
    [p [pcdata "Write a number: ";
        Form.input ~input_type:`Text ~name:intname Form.int;
        br ();
        Form.input ~input_type:`Submit ~value:"Send" Form.string]]
  in
  let f = Form.get_form calc_i create_form in
  Lwt.return (html (head (title (pcdata "")) []) (body [f]))


(* -------------------------------------------------------- *)
(* The handler for the service with parameter.              *)
(* It creates dynamically and registers a new coservice     *)
(* with one GET integer parameter.                          *)
(* This new coservice depends on the first value (i)        *)
(* entered by the user.                                     *)

let calc_i_handler i () =
  let create_form is =
    (fun entier ->
       [p [pcdata (is^" + ");
           Form.input ~input_type:`Text ~name:entier Form.int;
           br ();
           Form.input ~input_type:`Submit ~value:"Sum" Form.string]])
  in
  let is = string_of_int i in
  let calc_result =
    Eliom_registration.Html.create_attached_get
      ~scope:Eliom_common.default_session_scope
      ~fallback:calc
      ~get_params:(Eliom_parameter.int "j")
      ~timeout:120.
      (fun j () ->
        let js = string_of_int j in
        let ijs = string_of_int (i+j) in
        Lwt.return (html
             (head (title (pcdata "")) [])
             (body [p [pcdata (is^" + "^js^" = "^ijs)]])))
  in
  let f = Form.get_form calc_result (create_form is) in
  Lwt.return (html (head (title (pcdata "")) []) (body [f]))


(* -------------------------------------------------------- *)
(* Registration of main services:                           *)

let () =
  Eliom_registration.Html.register calc   calc_handler;
  Eliom_registration.Html.register calc_i calc_i_handler
