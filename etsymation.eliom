[%%shared
    open Utils
    open Eliom_lib
    open Eliom_content
    open Html.D
    open Translator
    open Login
    open Services
    open Listing_handler
]
module Etsymation_app =
  Eliom_registration.App (
    struct
      let application_name = "etsymation"
      let global_data_path = None
    end)

let user_service =  Eliom_content.Html.D.(
  Eliom_service.create
    ~path:(Eliom_service.Path ["users"])
    ~meth:(Eliom_service.Get
    (Eliom_parameter.(suffix (string "name"))))
      ()
)

let get_score name =
  try 
  let open Eliom_content.Html.D in
  let user_id = Db.get_id_from_name name in
  match user_id with
  | None -> p [pcdata "User not found."]
  | Some user_id ->
    let score = Db.get_total_score user_id in
    let score = string_of_int score in
    p ~a:[a_class ["is-size-1"]] [pcdata ("Score: " ^ score)]
  with
  | _ as e -> print_endline "score"; Printexc.print_backtrace (stdout);e |> Printexc.to_string |> print_endline; raise e
let () =
  Eliom_registration.Html.register
    ~service: user_service
    (fun name _ ->
        Skeleton.skeleton name @@
          [
             section ~a:[a_class["hero"; "is-primary"]] [
               div ~a:[a_class ["hero-body"]] [
                 div ~a:[a_class ["container"]] [
             h1
              ~a:[a_class ["title"]]
              [pcdata name];
               p [a ~service:main_service [pcdata "Home"] ()];
               get_score name
               ]]]])

let () =
  Etsymation_app.register
    ~service:main_service
    (fun () () ->
       let%lwt connection_form = connection_box () in
       (*let%lwt _ =
            Lwt.return @@
             Eliom_reference.set References.user_id (Some Int64.one);
             Eliom_reference.set References.username (Some "john") in
*)
        Skeleton.skeleton_f "etsymation" @@
          [
             section ~a:[a_class["hero"; "is-primary"]] [
               div ~a:[a_class ["hero-body"]] [
                 div ~a:[a_class ["container"]] [
             h1
              ~a:[a_class ["title"]]
              [pcdata "Welcome to The Etsymation Game!"];
             p [pcdata "Etsymation is a game inspired by the price
             is right, applied to Etsy furniture. Ready to play?"]]]];
             Html.C.node [%client Login.notif_node];
             connection_form;
           ]
)

(* let _ =
  (match Db.get_id_from_name "kino" with
  | Some _ -> div [pcdata "found"]
  | None -> div [pcdata "not found"]); *)

let _ =
    update_listings () ()

(* FIXME: Be called once per hour *)
let rec myself _ =
  let%lwt _ = Lwt_unix.sleep 3600.0 in
    update_listings () ();
    myself ()

let _ =
  myself ()
