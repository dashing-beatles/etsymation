let new_user_form_service =
  Eliom_service.create
    ~path:(Eliom_service.Path ["registration"])
    ~meth:(Eliom_service.Get Eliom_parameter.unit)
    ()

let create_account_service =
  Eliom_service.create_attached_post
    ~fallback:Services.main_service
    ~post_params:(Eliom_parameter.(string "name" ** string "password"))
    ()

let account_form = Eliom_content.Html.D.(
  Form.post_form
    ~service:create_account_service
      (fun (name, pwd) ->
        [fieldset
          [label [pcdata "login: "];
          Form.input
            ~input_type: `Text ~name:name Form.string;
          br ();
          label [pcdata "password: "];
          Form.input
            ~input_type: `Password ~name:pwd Form.string;
          br ();
          Form.input
            ~input_type: `Submit
            ~value: "Register" Form.string
        ]])
        ()
)

(* Service registration *)

let _ = Eliom_content.Html.D.(
  Eliom_registration.Html.register
    ~service:new_user_form_service
    (fun _ _ ->
      Lwt.return @@
      (html (head (title (pcdata "")) [])
            (body [h1 [pcdata "Create an account"];
                  account_form;])))
)

let _ = 
  Eliom_registration.Action.register
    ~service:create_account_service
    (fun _ (name, pwd) ->
    (* Database access here *)
      Lwt.return ()
      )